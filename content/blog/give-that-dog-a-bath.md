---
title: It's time to give your dog a bath
date: 2022-09-29
draft: false
type: blog
lastmod: 2022-09-29
description: Every dog needs a bath at some point, so you should know how to make it a good one.
preview: /uploads/doggie-bath.jpg
---

Just like little toddlers, dogs have active lifestyles filled with fun and mayhem. And just like toddlers, dogs have an uncanny ability to suddenly and unexpectedly get very, very messy. Every dog owner has experienced that certain magical moment when the dog has found (cough) something really gross to roll around in outside and it's time for an emergency bath.

Although washing your dog is admittedly a frustrating and thankless chore, there are some strategies you can use to make your next dog's bath as easy and painless as possible.


## Get a buddy to help you

Friends don't let friends wash dogs alone. Many unexpected surprises or difficulties could occur that can be more easily managed with an extra person around:

- Your dog might try to escape and you'll want someone to help keep it restrained in the bathtub.
- You might forget an important item like shampoo or a towel and will need someone to run and get it.
- Your dog might need some moral support during this difficult experience and could benefit from having one person who can talk to it in a calm, soothing voice while the person doing the washing is muttering vile curses under their breath.

For all these reasons and more, you'll appreciate having the support of at least one other person. If nothing else, washing the dog has the potential to bond two people together forever, much like soldiers bonding over the shared trauma of war.


## Find a location

If your dog is small enough, you might be able to bathe it in the kitchen sink. However, medium or larger dogs will mostly likely need to be bathed in a bathtub. Another nice benefit of washing your dog in the bathroom is that you can shut the door and prevent a possible escape attempt.

You can also visit your local [Petco](https://www.petco.com/shop/en/petcostore/s/dog-wash) store as they have many dog washing stations, too.


## Use the right supplies

Make sure you wear clothing that can get wet because your clothing will definitely get wet and you will likely need to change afterwards.

Before you put the dog in the tub, fill your bathtub halfway with lukewarm (not hot) water. After putting your dog in the water, use a hand-held shower nozzle, large cup, or plastic pitcher to get your dog completely wet.

Next, apply the shampoo and work up a good lather. If this is an emergency bath, you can use shampoo meant for humans. But if you have the time and money to do so, you'll want to buy shampoo that was specifically made for dogs. Human shampoo unfortunately contains ingredients that may irritate your dog's skin or stomach. Some breeds of dogs (such as retrievers) might have coats that could also benefit from a conditioner as well, but it isn't usually necessary. 

Don't forget to thoroughly rinse out all suds when you're finished.


## Dry your dog well, but do so with caution

As you drain the water and get ready to pull your dog from the bath, have your towel on hand to quickly wrap them with a towel. Use the towel to pat your dog down well, getting it as dry as possible. Never use a hair dryer to dry your dog as doing so could cause skin burns and hurt their ears.

What most articles about washing dogs might forget to warn you about is the fact that dogs have a very strong instinct to shake themselves dry when they are wet. Even if you've patted them down with a towel very thoroughly, that most likely will not be enough to prevent this instinct from kicking in.

For that reason, after removing the towel from your dog, you should take my advice and be prepared to run. You will get covered in water if you don't. Alternatively, if you took this blog entry's earlier advice to bring along a buddy to help wash your dog, you can now use them as a human shield.


---

After successfully bathing your pet, make sure you give your dog a treat and perhaps play tug-of-war with the towel to communicate that things are going back to normal now. No one will blame you if you also decide to reward yourself with a treat of some sort, such as winding down with a soothing glass of wine or listening to relaxing, non-canine nature sounds.